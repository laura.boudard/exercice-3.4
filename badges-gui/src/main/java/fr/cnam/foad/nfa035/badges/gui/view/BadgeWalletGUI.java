package fr.cnam.foad.nfa035.badges.gui.view;

import fr.cnam.foad.nfa035.badges.gui.components.BadgePanel;
import fr.cnam.foad.nfa035.badges.gui.context.ApplicationContextProvider;
import fr.cnam.foad.nfa035.badges.gui.controller.BadgesWalletController;
import fr.cnam.foad.nfa035.badges.gui.model.BadgesModel;
import fr.cnam.foad.nfa035.badges.gui.renderer.BadgeSizeCellRenderer;
import fr.cnam.foad.nfa035.badges.gui.renderer.DefaultBadgeCellRenderer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.List;
import java.util.*;

/**
 * Commentez-moi
 */
@Component("badgeWallet")
@Order(value=2)
public class BadgeWalletGUI{

    private JButton button1;
    private JPanel panelParent;
    private JTable table1;
    private JPanel panelImageContainer;
    private JScrollPane scrollBas;
    private JScrollPane scrollHaut;
    private JPanel panelHaut;
    private JPanel panelBas;

    List<DigitalBadge> tableList;

    private BadgePanel badgePanel;
    private BadgesModel tableModel;
    @Autowired
    private BadgesWalletController badgesWalletController;

    @Autowired
    private AddBadgeDialog addBadgeDialog;

    /**
     * Constructeur
     * Commentez-moi et décrivez le méchanisme de Forms
     */
    public BadgeWalletGUI() {

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addBadgeDialog.pack();
                addBadgeDialog.setLocationRelativeTo(null);
                addBadgeDialog.setIconImage(addBadgeDialog.getIcon().getImage());
                addBadgeDialog.setVisible(true);
            }
        });

        //TableModel tableModel = TableModelCreator.createTableModel(DigitalBadge.class, tableList);
        tableModel = new BadgesModel(tableList);
        badgePanel.setPreferredSize(scrollHaut.getPreferredSize());
        table1.setModel(tableModel);
        table1.setRowSelectionInterval(0, 0);

        // 2. Les Renderers...
        table1.getColumnModel().getColumn(4).setCellRenderer(new BadgeSizeCellRenderer());
        table1.setDefaultRenderer(Object.class, new DefaultBadgeCellRenderer());
        // Apparemment, les Objets Number sont traités à part, donc il faut le déclarer explicitement en plus de Object
        table1.setDefaultRenderer(Number.class, new DefaultBadgeCellRenderer());
        // Idem pour les Dates
        table1.setDefaultRenderer(Date.class, new DefaultBadgeCellRenderer());

        // 3. Tri initial
        table1.getRowSorter().toggleSortOrder(0);

        table1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                super.mouseClicked(event);
                if (event.getClickCount() == 2) {
                    int row = ((JTable) event.getComponent()).getSelectedRow();
                    badgesWalletController.loadBadge(row);
                }
            }
        });
    }

    public JTable getTable1() {
        return table1;
    }

    public JPanel getPanelImageContainer() {
        return panelImageContainer;
    }

    public JScrollPane getScrollHaut() {
        return scrollHaut;
    }

    public JPanel getPanelHaut() {
        return panelHaut;
    }

    public List<DigitalBadge> getTableList() {
        return tableList;
    }

    public BadgesModel getTableModel() {
        return tableModel;
    }

    public void setPanelImageContainer(JPanel panelImageContainer) {
        this.panelImageContainer = panelImageContainer;
    }

    public void setTableList(List<DigitalBadge> tableList) {
        this.tableList = tableList;
    }

    public void setBadgePanel(BadgePanel badgePanel) {
        this.badgePanel = badgePanel;
    }

    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/logo.png"));
    }

        private void createUIComponents() {
            try {
                this.badgesWalletController = ApplicationContextProvider.getApplicationContext().getBean("badgesWalletController", BadgesWalletController.class);

                badgesWalletController.delegateUIComponentsCreation(this);
                badgesWalletController.delegateUIManagedFieldsCreation(this);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }


    /**
     * Commentez-moi
     * @param displayedBadgeHolder
     */
    public void delegateSetAddedBadge(DisplayedBadgeHolder displayedBadgeHolder) {

        tableModel.addBadge(displayedBadgeHolder);
        tableModel.fireTableDataChanged();
        badgesWalletController.loadBadge(tableModel.getRowCount()-1);
    }



    public Container getPanelParent() { return panelParent;}
}

package fr.cnam.foad.nfa035.badges.gui.controller;


import fr.cnam.foad.nfa035.badges.gui.components.BadgePanel;
import fr.cnam.foad.nfa035.badges.gui.components.BadgesPanelFactory;
import fr.cnam.foad.nfa035.badges.gui.model.BadgesWalletDAOFactory;
import fr.cnam.foad.nfa035.badges.gui.view.AddBadgeDialog;
import fr.cnam.foad.nfa035.badges.gui.view.BadgeWalletGUI;
import fr.cnam.foad.nfa035.badges.gui.view.DisplayedBadgeHolder;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;

@Component("badgeWalletController")
public class BadgesWalletController {

    DirectAccessBadgeWalletDAO dao;
    DisplayedBadgeHolder displayedBadgeHolder;
    BadgesPanelFactory badgesPanelFactory;

    BadgesWalletDAOFactory modelFactory;
    BadgeWalletGUI vueBadgeWallet;
    AddBadgeDialog addBadgeDialog;

    public BadgesWalletController(DirectAccessBadgeWalletDAO dao, DisplayedBadgeHolder displayedBadgeHolder, BadgesPanelFactory badgesPanelFactory) {
        this.dao = dao;
        this.displayedBadgeHolder = displayedBadgeHolder;
        this.badgesPanelFactory = badgesPanelFactory;

    }

    public BadgesWalletController(BadgesWalletDAOFactory modelFactory, BadgeWalletGUI vueBadgeWallet, AddBadgeDialog addBadgeDialog) {
        this.modelFactory = modelFactory;
        this.vueBadgeWallet = vueBadgeWallet;
        this.addBadgeDialog = addBadgeDialog;
    }

    /**
     * Permet de charger le badge
     * @param row
     */
    @Autowired
    public void loadBadge(int row){

        addBadgeDialog.panelHaut.removeAll();
        panelHaut.revalidate();

        badge = tableList.get(row);
        createUIComponents();
        table1.setRowSelectionInterval(row, row);
        panelHaut.add(scrollHaut);
        panelImageContainer.setPreferredSize(new Dimension(256, 256));
        scrollHaut.setViewportView(panelImageContainer);

        panelHaut.repaint();
    }
    @Autowired
     public void setAddedBadge (DisplayedBadgeHolder displayedBadgeHolder) {
            this.displayedBadgeHolder = displayedBadgeHolder;
     }


    /**
     * délégation de création des composants de l'interface utilisateur
     * @param badgeWalletGUI
     */
    public void delegateUIComponentsCreation(BadgeWalletGUI badgeWalletGUI) {
        ;
    }

    /**
     * délégation de création des champs de l'interface utilisateur
     * @param badgeWalletGUI
     */
    public void delegateUIManagedFieldsCreation(BadgeWalletGUI badgeWalletGUI) {
            this.badgePanel = new BadgePanel(badge, dao);
            this.badgePanel.setPreferredSize(new Dimension(256, 256));
            this.panelImageContainer = new JPanel();
            this.panelImageContainer.add(badgePanel);
    }
}

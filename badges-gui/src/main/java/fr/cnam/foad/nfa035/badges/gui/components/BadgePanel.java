package fr.cnam.foad.nfa035.badges.gui.components;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Component("badgePanel")
@ComponentScan("fr.cnam.foad.nfa035.badges")
public class BadgePanel extends JPanel {


    private DirectAccessBadgeWalletDAO dao;
    private DigitalBadge badge;

    public void setBadge(DigitalBadge badge) {this.badge = badge;}
    public DigitalBadge getBadge() {return badge;}



    @Autowired
    public BadgePanel(DigitalBadge badge, DirectAccessBadgeWalletDAO dao) {
        super();
        this.badge = badge;
        this.dao = dao;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            dao.getBadgeFromMetadata(bos, badge);
            g.drawImage(ImageIO.read(new ByteArrayInputStream(bos.toByteArray())), 0, 0, this.getWidth(), this.getHeight(), this);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }


}
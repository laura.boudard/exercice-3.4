package fr.cnam.foad.nfa035.badges.gui.components;


import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;


public final class BadgesPanelFactory {

    private static final BadgesPanelFactory INSTANCE = new BadgesPanelFactory();

    /**
     * Constructeur
     */
    public BadgesPanelFactory() {
        super();
    }

    private DigitalBadge badge;
    private DirectAccessBadgeWalletDAO dao;
    /**
     * Renvoie l'instance unique de la clase
     */
    public static BadgesPanelFactory getInstance () {
        return BadgesPanelFactory.INSTANCE;
    }

    public DigitalBadge getBadge () {return badge;}

    public DirectAccessBadgeWalletDAO getDao () {return dao;}

    public void setBadge(DigitalBadge badge) {this.badge = badge;}
}
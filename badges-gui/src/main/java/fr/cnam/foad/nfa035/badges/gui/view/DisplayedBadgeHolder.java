package fr.cnam.foad.nfa035.badges.gui.view;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

public class DisplayedBadgeHolder {

    private DigitalBadge displayBadge;

    public DigitalBadge getDisplayBadge() {
        return displayBadge;
    }

    public void setDisplayBadge(DigitalBadge displayBadge) {
        this.displayBadge = displayBadge;
    }
}

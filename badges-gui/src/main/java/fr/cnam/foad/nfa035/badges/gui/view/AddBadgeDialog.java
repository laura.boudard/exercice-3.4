package fr.cnam.foad.nfa035.badges.gui.view;

import fr.cnam.foad.nfa035.badges.gui.controller.AddBadgeDialogController;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.jdesktop.swingx.JXDatePicker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * Commentez-moi
 */
@Component("addBadgeDialog")
public class AddBadgeDialog extends JDialog implements PropertyChangeListener {

    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField codeSerie;
    private JFileChooser fileChooser;

    private JXDatePicker dateDebut;
    private JXDatePicker dateFin;

    BadgeWalletGUI badgeWallet;
    DisplayedBadgeHolder displayedBadgeHolder;
    AddBadgeDialogController addBadgeController;


    @Autowired
    public AddBadgeDialog() {

        fileChooser.setFileFilter(new FileNameExtensionFilter("Petites images *.jpg,*.png,*.jpeg,*.gif", "jpg","png","jpeg","gif"));
        fileChooser.addPropertyChangeListener(this);
        codeSerie.addPropertyChangeListener(this);
        dateDebut.addPropertyChangeListener(this);
        dateFin.addPropertyChangeListener(this);

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }


    public JFileChooser getFileChooser() {return fileChooser;}

    public JXDatePicker getDateDebut() {return dateDebut;}

    public JXDatePicker getDateFin() {return dateFin;}

    /**
     * Commentez-moi
     */
    private void onOK() {
        badgeWallet.delegateSetAddedBadge(displayedBadgeHolder.getDisplayBadge());
        dispose();
    }

    /**
     * Commentez-moi
     */
    private void onCancel() {
        dispose();
    }

    /**
     * Commentez-moi
     * @param args
     */
    public static void main(String[] args) {
        AddBadgeDialog dialog = new AddBadgeDialog();
        dialog.pack();
        dialog.setLocationRelativeTo(null);
        dialog.setIconImage(dialog.getIcon().getImage());
        dialog.setVisible(true);
        System.exit(0);
    }
    @Autowired
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/logo.png"));
    }

    /**
     * {@inheritDoc}
     * @param evt
     */
    @Autowired
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (addBadgeController.validateForm(this)){
            this.buttonOK.setEnabled(true);
        }
        else{
            this.buttonOK.setEnabled(false);
        }
    }
    public JTextField getCodeSerie() {return codeSerie;}


}
